/*
 =================================
 img-layers-touch-canvas

 (c) 2021 Pierre Caruel


 based on img-touch-canvas - v0.1
 http://github.com/rombdn/img-touch-canvas
 
 (c) 2013 Romain BEAUDON
 This code may be freely distributed under the MIT License
 =================================
 */


(function () {
    var root = this; //global object

    var ImgTouchCanvas = function (options) {
        if (!options || !options.canvas) {
            throw 'ImgZoom constructor: missing arguments canvas';
        }

        this.canvas = options.canvas;
        this.canvas.width = this.canvas.clientWidth;
        this.canvas.height = this.canvas.clientHeight;
        this.context = this.canvas.getContext('2d');
        this.desktop = options.desktop || false; //non touch events        
        if (options.wait_image !== undefined) {
            this.waitImage = new Image();
            this.waitImage.src = options.wait_image;
        }
        if (options.background_color !== undefined) {
            this.background_color = options.background_color;
        }

        this.all_loaded = 2; // 0 : none, 1 : all visible, 2: all
        this.is_animating = false; // tell an display update is running
        this.pending_animation = false; // tell a request for an update is arrived while animating
        setInterval(this.updateAnimation.bind(this), 250);
        this.waiting = false;
        this.wait_rotate_angle = 0;

        this.centre = {
            x: 0,
            y: 0
        };
        this.scale = {
            x: 0.5,
            y: 0.5
        };
        this.current_zoom_level = 1;
        this.init = false;
        this.imgLayers = []; // array of image object corresponding to the layer
        this.visibility = []; // layer visibility
        this.opacity = [];
        this.layerNames = [];
        this.background_loaded = false;
        this.img_loaded = {};
        if (options.background !== undefined) {
            this.setBackground(options.background);
        }

        if (options.layers !== undefined) {
            this.setLayers(options.layers);
        }

        this.lastZoomScale = null;
        this.lastX = null;
        this.lastY = null;
        this.mdown = false; //desktop drag

        // request updated position / size by the user
        this.request_centre = {};
        this.request_centre.x = -1;
        this.request_centre.y = -1;
        this.request_width = -1;
        this.request_height = -1;
        //this.checkRequestAnimationFrame();
        //requestAnimationFrame(this.animate.bind(this));
        this.setEventListeners();
    };
    ImgTouchCanvas.prototype = {
        setVisibility: function (name, visible) {
            var i = 0;
            var r = -1;
            this.layerNames.forEach(function (l) {
                if (l === name) {
                    r = i;
                }
                i++;
            });
            if (r != -1) {
                this.visibility[r] = visible;
            }
            this.animate();
        },
        loadImage: function (img, src, visible, zoom, background) { // load image in given Image object, with given zoom level (if any), visible or not, store information in download queue, order by priority. Zoom == 0 when only 1 value of zoom==1 is geven
            if (this.img_loaded[src] !== undefined) {// image already loaded or loading, then do nothing
                return {status: 2};
            }
            var priority = 3;
            if (this.init) { // background already loaded
                if (zoom == this.current_zoom_level || zoom == 0) {
                    if (visible) {
                        priority = 1;
                    }
                } else if (visible) {
                    priority = 2;
                }
            } else { // background
                priority = 0;
            }
            // status: 
            // 0 : loading
            // 1 : waiting
            // 2 : loaded
            // 3 : failed load
            var status = 1;
            if (this.img_loaded[src] !== undefined) { // already in map, then re-use status
                status = this.img_loaded[src].status;
                if (status < 2) {
                    this.all_loaded = 0;
                }
            } else {
                this.all_loaded = 0;
            }
            var o = {image: img, visible: visible, zoom: zoom, priority: priority,
                status: status, background: background};
            this.img_loaded[src] = o; // associative array indexed with src filename
            return o;
        },
        loadAllImages: function () { // load all pending images
            var all_loaded = true;
            var all_visible_loaded = true;
            for (var priority = 0; priority < 4; priority++) { // highest priority first
                Object.keys(this.img_loaded).forEach((function (src) {
                    var i = this.img_loaded[src];
                    if (i.status === 1 && priority === i.priority) {
                        i.image.src = src;
                        if (i.image.complete) {
                            i.status = 2;
                        } else {
                            i.status = 0; // set as loading
                        }
                    }
                    if (i.status < 2) {
                        all_loaded = false;
                        if ((this.current_zoom_level == i.zoom || i.zoom == 0) && i.visible
                                ) {
                            all_visible_loaded = false;
                        }
                    }
                }).bind(this));
            }
            if (all_loaded) {
                this.all_loaded = 2;
            } else if (all_visible_loaded) {
                this.all_loaded = 1;
            }
            if (all_visible_loaded) {
                this.animate();
            }
        },
        allLoaded: function (only_visible = true) { // tell if everything (or only visible) is loaded
            if (this.all_loaded) {
                return this.all_loaded;
            }
            var all_loaded = true;
            var all_visible_loaded = true;
            Object.keys(this.img_loaded).forEach((function (src) {
                var i = this.img_loaded[src];
                if (i.image.complete && i.image.src !== "") {
                    i.status = 2;
                }
                if (i.status < 2) {
                    if ((!only_visible || i.visible) &&
                            (i.zoom == this.current_zoom_level || i.zoom == 0)) {
                        all_visible_loaded = false;
                    }
                    all_loaded = false;
                }
            }).bind(this));
            if (all_loaded) {
                this.all_loaded = 2; // all loaded
            } else if (all_visible_loaded) {
                this.all_loaded = 1; // all visible only
            }
            return this.all_loaded;
        },
        loadFirstBackground: function () { // load background at zoom = 1, when loaded it will trigger the load of the rest of the images
            Object.keys(this.img_loaded).forEach((function (src) {
                var i = this.img_loaded[src];
                if (i.background && (i.zoom == 1 || i.zoom == 0)) {
                    i.status = 0; // set as loading
                    i.image.addEventListener('load', this.loaded.bind(this), false);
                    i.image.addEventListener('load', this.doInit.bind(this), false);
                    i.image.src = src;
                    if (i.image.complete) {
                        i.status = 2;
                        this.doInit(); // first background loaded
                    }
                }
            }).bind(this));
        },
        changeLayer: function (name, src, zoom) {
            var i = 0;
            var r = -1;
            this.layerNames.forEach(function (l) {
                if (l === name) {
                    r = i;
                }
                i++;
            });
            if (r != -1) {
                if (zoom === undefined) { // multiple zoom levels
                    if (this.imgLayers[r].src === undefined) { // multiple zoom level defined
                        Object.keys(src).forEach(function (z) {
                            if (this.img_loaded[src[z]] === undefined) { // image not loaded, then load (create object in img_loaded)
                                // create new image object                                
                                this.imgLayers[r][z] = new Image();
                                this.imgLayers[r][z].addEventListener('load', this.loaded.bind(this), false);

                                this.loadImage(this.imgLayers[r][z], src[z], this.visibility[r], z);
                            } else { // use image object stored in img_loaded into imgLayer
                                this.imgLayers[r][z] = this.img_loaded[src[z]].image;
                            }
                        }.bind(this));
                    } else {
                        if (this.img_loaded[src] === undefined) {
                            // create new image object
                            this.imgLayers[r] = new Image();
                            this.imgLayers[r].addEventListener('load', this.loaded.bind(this), false);
                            this.loadImage(this.imgLayers[r], src, this.visibility[r], 0); //TODO 1 or undefined for zoom
                        } else {
                            this.imgLayers[r] = this.img_loaded[src].image;
                        }
                    }
                } else { // single zoom level
                    if (this.img_loaded[src] === undefined) {
                        // create new image object
                        this.imgLayers[r] = new Image();
                        this.imgLayers[r].addEventListener('load', this.loaded.bind(this), false);
                        this.loadImage(this.imgLayers[r], src, this.visibility[r], 0);
                    } else {
                        this.imgLayers[r] = this.img_loaded[src].image;
                    }
                    //this.loadImage(this.imgLayers[r], src, this.visibility[r], 0);
                }
            }
        },
        setBackground: function (src) {
            var zoom_levels = Object.keys(src);
            if (zoom_levels.length === 0) { // single zoom level
                this.imgBackground = new Image();
                this.imgBackground.addEventListener('load', this.loaded.bind(this), false);
                this.loadImage(this.imgBackground, src, true, 0, true);
            } else {
                this.imgBackground = {};
                zoom_levels.forEach((function (z) {
                    this.imgBackground[z] = new Image();
                    this.imgBackground[z].addEventListener('load', this.loaded.bind(this), false);
                    this.loadImage(this.imgBackground[z], src[z], true, z, true);
                }).bind(this));
            }
        },
        nearestZoomLevel: function (zoom_level) { // return which version of zoom to use
            var zoom_l = zoom_level === undefined ? this.scale.x : zoom_level;
            var z;
            var zoom_levels = Object.keys(this.imgBackground);
            if (zoom_levels.length) {
                for (var i = 0; i < zoom_levels.length; i++) {
                    var zl = zoom_levels[i];
                    if (zoom_l <= zl) {
                        z = zl;
                        break;
                    }
                }
                if (z === undefined) {
                    z = zoom_levels[zoom_levels.length - 1]; // use highest 
                }
            } else {
                z = 1;
            }
            return z;
        },
        setLayers: function (layers) {
            this.imgLayers = [];
            this.visibility = []; // layer visibility
            this.opacity = [];
            this.layerNames = [];
            var all_loaded = true; // indicate all images are already loaded
            var all_visible_loaded = true; // indicate all images are already loaded
            for (var i = 0; i < layers.length; i++) {
                this.opacity.push(layers[i].opacity);
                this.layerNames.push(layers[i].name);
                this.visibility.push(layers[i].visibility);
                if (typeof (layers[i].src) == "string") { // single zoom level, src == string
                    if (this.img_loaded[layers[i].src] !== undefined) { // already loaded from a previous call to setLayers
                        this.imgLayers.push(this.img_loaded[layers[i].src].image);
                    } else { // create image
                        var img = new Image();
                        img.addEventListener('load', this.loaded.bind(this), false);
                        this.imgLayers.push(img);
                        var o = this.loadImage(img, layers[i].src, layers[i].visibility, 0);
                        if (o.status != 2) {
                            if (o.visible && (o.zoom == this.current_zoom_level || o.zoom == 0)) {
                                all_visible_loaded = false;
                            }
                            all_loaded = false;
                        }
                    }
                } else { // go through all zoom levels
                    var zoom_levels = Object.keys(layers[i].src);
                    var images = {};
                    zoom_levels.forEach((function (z) {
                        if (this.img_loaded[layers[i].src[z]] !== undefined) { // already loaded from a privous call to setLayers
                            images[z] = this.img_loaded[layers[i].src[z]].image;
                        } else { // create image
                            var img = new Image();
                            img.addEventListener('load', this.loaded.bind(this), false);
                            images[z] = img;
                            var o = this.loadImage(img, layers[i].src[z], layers[i].visibility, z);
                            if (o.status != 2) {
                                if (o.visible && (o.zoom == this.current_zoom_level || o.zoom == 0)) {
                                    all_visible_loaded = false;
                                }
                                all_loaded = false;
                            }
                        }
                    }).bind(this));
                    this.imgLayers.push(images);
                }
            }
            if (all_visible_loaded) {
                if (all_loaded) {
                    this.all_loaded = 2;
                } else {
                    this.all_loaded = 1;
                }
                this.doInit();
                this.animate(); // if all images are loaded, load event will not be fired, so we need to manually animate
            } else {
                this.all_loaded = 0;
                if (this.init) { // background @ zoom == 1 already loaded
                    this.loadAllImages();
                } else {
                    this.loadFirstBackground(); // load background @ zoom == 1
                }
            }
        },
        loaded: function (e) { // image is loaded (called by event)
            var o = this.imageLoaded(e.currentTarget.src);

            if (o !== undefined) {
                o.status = 2; // loaded
                if (o.visible && (o.zoom == this.current_zoom_level || o.zoom == 0)) { // animate if this image is visible, and at current zoom level
                    if (this.is_animating) { // already running
                        this.pending_animation = true; // tell to wait for animation to re-run it
                    } else {
                        this.animate();
                    }
                }
            }
        },
        imageLoaded: function (src) { // return image loaded object {img, src, zoom, status} for given src (or part of it)
            var image;
            Object.keys(this.img_loaded).forEach((function (i) {
                if (src.endsWith(i)) {
                    image = this.img_loaded[i];
                }
            }).bind(this));
            return image;
        },
        doInit: function (e) {
            var background_1; // bakcground for zoom = 1
            var zoom = this.nearestZoomLevel();
            if (Object.keys(this.imgBackground).length) {
                background_1 = this.imgBackground[1];
            } else {
                background_1 = background;
            }
            //set scale such as image cover all the canvas
            if (!this.init && background_1) {
                if (background_1.width) {
                    var scale_ratio_x = this.canvas.width / background_1.width;
                    var scale_ratio_y = this.canvas.height / background_1.height;
                    var scaleRatio = scale_ratio_x < scale_ratio_y ?
                            scale_ratio_x : scale_ratio_y;
                    this.scale.x = scaleRatio;
                    this.scale.y = scaleRatio;
                    zoom = this.nearestZoomLevel();
                    this.current_zoom_level = zoom;
                    this.init = true;
                    this.centre.x = background_1.width / 2; // coordinate of the centre of the image for zoom = 1
                    this.centre.y = background_1.height / 2; // coordinate of the centre of the image for zoom = 1

                    // update priority with updated zoom level
                    Object.keys(this.img_loaded).forEach(function (o) {
                        if (o.zoom == this.current_zoom_level || o.zoom == 0) {
                            if (o.visible) {
                                o.priority = 1;
                            }
                        } else if (o.visible) {
                            o.priority = 2;
                        }
                    });
                    // if specified by user, update centre position
                    this.doCentre();
                    this.loadAllImages();
                }
            }
        },
        animate: function (check_all_loaded = true) {
            this.is_animating = true; // lock
            var background;
            var background_1; // bakcground for zoom = 1
            var zoom = this.nearestZoomLevel();
            this.current_zoom_level = zoom;
            if (Object.keys(this.imgBackground).length) {
                background = this.imgBackground[zoom];
                background_1 = this.imgBackground[1];
            } else {
                background = this.imgBackground;
                background_1 = background;
            }

            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            if (!background.width) { // not loaded
                this.animateWait(true); // wait image
                this.is_animating = false;
                return;
            }
            do {
                this.pending_animation = false;

                if (this.imgBackground && this.init && this.imageLoaded(background.src).status == 2) {
                    if (this.background_color !== undefined) {
                        this.context.fillStyle = this.background_color;
                    }
                    this.context.fillRect(-this.centre.x * this.scale.x + this.canvas.width / 2, -this.centre.y * this.scale.y + this.canvas.height / 2,
                            this.scale.x * background_1.width,
                            this.scale.y * background_1.height);
                    this.context.drawImage(
                            background,
                            -this.centre.x * this.scale.x + this.canvas.width / 2, -this.centre.y * this.scale.y + this.canvas.height / 2,
                            this.scale.x * background_1.width,
                            this.scale.y * background_1.height);
                    for (var i = 0; i < this.imgLayers.length; i++) {
                        var image;
                        if (Object.keys(this.imgLayers[i]).length) {
                            image = this.imgLayers[i][zoom];
                            if (image === undefined) {
                                image = this.imgLayers[i][1];
                            }
                        } else {
                            image = this.imgLayers[i];
                        }
                        var s = this.imageLoaded(image.src);
                        if (this.visibility[i] && image.src.length > 0 && s.status == 2) { // layer visible
                            this.context.save();
                            this.context.globalAlpha = this.opacity[i] / 100;
                            this.context.drawImage(
                                    image,
                                    -this.centre.x * this.scale.x + this.canvas.width / 2, -this.centre.y * this.scale.y + this.canvas.height / 2,
                                    this.scale.x * background_1.width,
                                    this.scale.y * background_1.height);
                            this.context.restore();
                        }
                    }
                    if (check_all_loaded) {
                        if (this.allLoaded() == 0) {
                            this.animateWait(true); // wait image until everything is loaded
                        } else {
                            this.animateWait(false);
                        }
                    }
                    if (this.waiting && this.waitImage !== undefined) {
                        this.context.save();
                        var width = this.canvas.width / 16;
                        var height = width * this.waitImage.height / this.waitImage.width;
                        this.context.translate(width / 2, height / 2);
                        this.context.rotate(this.wait_rotate_angle * (Math.PI / 180));
                        this.context.drawImage(this.waitImage, -width / 2, -height / 2, width, height);
                        this.context.restore();
                    }
                }
            } while (this.pending_animation); // if a request for animation arrived during the rendering, we start a new loop
            this.is_animating = false;
            //requestAnimationFrame(this.animate.bind(this));
        },
        updateAnimation: function () {
            if (this.waiting && !this.is_animating) {
                this.animate(false);
                this.wait_rotate_angle += 60;
                if (this.wait_rotate_angle >= 360) {
                    this.wait_rotate_angle -= 360;
                }
            }
        },
        animateWait: function (start) {
            this.waiting = start;

        }, gesturePinchZoom: function (event) {
            var zoom = false;
            if (event.targetTouches.length >= 2) {
                var p1 = event.targetTouches[0];
                var p2 = event.targetTouches[1];
                var zoomScale = Math.sqrt(Math.pow(p2.pageX - p1.pageX, 2) + Math.pow(p2.pageY - p1.pageY, 2)); //euclidian distance

                if (this.lastZoomScale) {
                    zoom = zoomScale - this.lastZoomScale;
                }

                this.lastZoomScale = zoomScale;
            }

            return zoom;
        },
        doZoom: function (zoom) {
            if (!zoom || !this.init)
                return;
            //new scale
            var currentScale = this.scale.x;
            var new_scale = this.scale.x + zoom / 100;
            this.current_zoom_level = this.nearestZoomLevel(new_scale);
            // everything is calculated against zoom=1 background size
            var background;
            if (Object.keys(this.imgBackground).length) {
                background = this.imgBackground[1];
            } else {
                background = this.imgBackground;
            }
            if (background.width * new_scale < this.canvas.width && background.height * new_scale < this.canvas.height) { // image smaller than canvas
                var scale_ratio_x = this.canvas.width / background.width;
                var scale_ratio_y = this.canvas.height / background.height;
                new_scale = scale_ratio_x < scale_ratio_y ?
                        scale_ratio_x : scale_ratio_y;                
            }
            //finally affectations
            this.scale.x = new_scale;
            this.scale.y = new_scale;
        },
        doMove: function (relativeX, relativeY) {
            if (this.init && this.lastX !== null && this.lastY !== null) {
                var deltaX = relativeX - this.lastX;
                var deltaY = relativeY - this.lastY;
                var background;
                if (Object.keys(this.imgBackground).length) {
                    background = this.imgBackground[1];
                } else {
                    background = this.imgBackground;
                }
                var cx = this.centre.x - deltaX / this.scale.x;
                var cy = this.centre.y - deltaY / this.scale.y;
                //edge cases
                if (background.width * this.scale.x > this.canvas.width) { // if image is wider than canvas, then check lateral boundaries
                    if (-cx * this.scale.x + this.canvas.width / 2 > 0) {
                        cx = this.canvas.width / (2 * this.scale.x);
                    }
                    if ((background.width - cx) * this.scale.x < this.canvas.width / 2) {
                        cx = background.width - this.canvas.width / (2 * this.scale.x);
                    }
                }
                if (background.height * this.scale.y > this.canvas.height) { // if image is higher than canvas, then check lateral boundaries
                    if (-cy * this.scale.y + this.canvas.height / 2 > 0) {
                        cy = this.canvas.height / (2 * this.scale.y);
                    }
                    if ((background.height - cy) * this.scale.y < this.canvas.height / 2) {
                        cy = background.height - this.canvas.height / (2 * this.scale.y);
                    }
                }
                this.centre.x = cx;
                this.centre.y = cy;
            }

            this.lastX = relativeX;
            this.lastY = relativeY;
        },
        centreOn: function (cx, cy, width, height) { // centre on rectangle (in zoom = 1 coordinates) and fit width /height 
            this.request_centre.x = cx;
            this.request_centre.y = cy;
            this.request_width = width;
            this.request_height = height;
            this.doCentre();
        },
        doCentre: function () { // centre on pre-entered values, called after init          
            if (this.request_width == -1 || this.request_height == -1) {
                return;
            }
            this.centre.x = this.request_centre.x;
            this.centre.y = this.request_centre.y;
            var scale_x = this.canvas.width / this.request_width;
            var scale_y = this.canvas.height / this.request_height;
            var scale_ratio = scale_x < scale_y ?
                    scale_x : scale_y;
            this.scale.x = scale_ratio;
            this.scale.y = scale_ratio;
            this.current_zoom_level = this.nearestZoomLevel();
            this.animate();
        },
        setEventListeners: function () {
            // touch
            this.canvas.addEventListener('touchstart', function (e) {
                this.lastX = null;
                this.lastY = null;
                this.lastZoomScale = null;
            }.bind(this));
            this.canvas.addEventListener('touchmove', function (e) {
                e.preventDefault();
                if (e.targetTouches.length == 2) { //pinch
                    this.doZoom(this.gesturePinchZoom(e));
                } else if (e.targetTouches.length == 1) {
                    var relativeX = e.targetTouches[0].pageX - this.canvas.getBoundingClientRect().left;
                    var relativeY = e.targetTouches[0].pageY - this.canvas.getBoundingClientRect().top;
                    this.doMove(relativeX, relativeY);
                }
                this.animate();
            }.bind(this));
            if (this.desktop) {
                // keyboard+mouse
                window.addEventListener('keyup', function (e) {
                    if (e.keyCode == 187 || e.keyCode == 61) { //+
                        this.doZoom(5);
                    } else if (e.keyCode == 54) {//-
                        this.doZoom(-5);
                    }
                    this.animate();
                }.bind(this));
                window.addEventListener('mousedown', function (e) {
                    this.mdown = true;
                    this.lastX = null;
                    this.lastY = null;
                }.bind(this));
                window.addEventListener('mouseup', function (e) {
                    this.mdown = false;
                }.bind(this));
                window.addEventListener('mousemove', function (e) {
                    var relativeX = e.pageX - this.canvas.getBoundingClientRect().left;
                    var relativeY = e.pageY - this.canvas.getBoundingClientRect().top;
                    if (e.target == this.canvas && this.mdown) {
                        this.doMove(relativeX, relativeY);
                        this.animate();
                    }

                    if (relativeX <= 0 || relativeX >= this.canvas.getBoundingClientRect().width || relativeY <= 0 || relativeY >= this.canvas.getBoundingClientRect().height) {
                        this.mdown = false;
                    }
                }.bind(this));
            }
        }
    };
    root.ImgTouchCanvas = ImgTouchCanvas;
}).call(this);