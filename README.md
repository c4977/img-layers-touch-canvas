img-layers-touch-canvas
=======================

img-layers-touch-canvas is a simple, dependency-free javascript library to display images in a canvas.

Features
========
+ Supports of gesture (scroll, zoom) for mobile devices
+ Supports multiple layers
+ For each layer, several images can be provided for different zoom level, allowing to display more refined content when image is zoomed-in.
+ All of the html-supported images (bitmap or vector) are allowed (png, jpg, svg).

See demo folder (weather map example) for documentation.

Derived from img-touch-canvas - v0.1
http://github.com/rombdn/img-touch-canvas
 
(c) 2013 Romain BEAUDON

License
=======
MIT

Author
======
Pierre Caruel
