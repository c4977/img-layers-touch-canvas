/*
 =================================
 img-layers-touch-canvas
 
 (c) 2021 Pierre Caruel
 Freely usable / redistributable under the terms of the MIT License
 
 */

var gesturable_img;

function start() {

    /* adjust canvas size to window
     * 
     */
    document.getElementById('maps-canvas').width = window.innerWidth;
    document.getElementById('maps-canvas').height = window.innerHeight - 100;

    /* define backgrounds as an object with url associated to each zoom level, or a single url
     * if background is provided only for default zoom level*/

    var backgrounds = {
        1: "background-transparent.png", 2: "background-transparent2.png", 4: "background-transparent4.png"};

    /* layers are defined in an array of object
     * each object represents a layer
     * each layer objects has visibility, opacity (0 to 100), and source, either
     * a url if valid for each zoom level, or an object with an url for each provided zoom level
     * (below : 1, 2 and 4)
     */

    var image_layers = [
        {"visibility": true, "opacity": 100, "src": {
                1: "wind.svg", 2: "wind2.svg", 4: "wind4.svg"},
            "name": "wind"},
        {"visibility": true, "opacity": 50, "src": "lcc.png",
            "name": "clouds"},
        {"visibility": true, "opacity": 50, "src": {
                1: "overlay.svg", 2: "overlay2.svg", 4: "overlay4.svg"},
            "name": "overlay"}
    ];

    // we use the width of the screen to decide if we are on a mobile device
    var narrow_screen = window.innerWidth < 800;

    /* construct ImgTouchCanvas instance with :
     * canvas: dom element of canvas to display to
     * desktop : true for a computer-type client with a mouse, false for a mobile-type client
     * with only gestures
     * background: background definition object (see above)
     * layers: layers definition object (see above)
     * wait_image: image to be animated when loading
     * background_color: color to fill the canvas with before drawing the first layer
     */

    gesturable_img = new ImgTouchCanvas({canvas: document.getElementById('maps-canvas'),
        desktop: !narrow_screen,
        background: backgrounds,
        layers: image_layers,
        wait_image: "wait.svg",
        background_color: "rgb(115,163,255)"}
    );

    /* alernatively, layers cat be specified by :
     gesturable_img.setLayers(image_layers);
     */

    /* call loadAllImages to initiate loading of all of the needed images (even those not 
     visible */
    gesturable_img.loadAllImages();
}

function zoomIn() {
    // change zoom (add 25%)
    gesturable_img.doZoom(25);
    // redraw
    gesturable_img.animate();
}

function zoomOut() {
    // change zoom (remove 25%)
    gesturable_img.doZoom(-25);
    // redraw
    gesturable_img.animate();
}

function changeVisibility(e) {
    /* change visibility with name of the layer and true or false
     * setVisibility(name, true | false)
     * 
     */
    gesturable_img.setVisibility(e.id, e.checked);
}

function changeLayerContent(e) {

    /* layers can be changed at any time, by calling change layer method with name of the layer to
     * be replaced (name must be as supplied in the layer object), and by either an image url :
     * changeLayer(name, src, 0)
     * or an object with url for each zoom level :
     * changeLayer(name, {1: url_zoom_1, 2: url_zoom_2, 4: url_zoom_4})
     * */
    gesturable_img.changeLayer("clouds", e.value + ".png", 0);
    gesturable_img.loadAllImages();
}
